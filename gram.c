/* A Bison parser, made by GNU Bison 3.7.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2020 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.7"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 2

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* First part of user prologue.  */
#line 1 "/home/esr/public_html/cvs-fast-export//gram.y"

/*
 *  Copyright © 2006 Keith Packard <keithp@keithp.com>
 *
 *  SPDX-License-Identifier: GPL-2.0+
 */

#include "cvs.h"
#include "gram.h"
#include "lex.h"

extern void yyerror(yyscan_t, cvs_file *, const char *);

extern YY_DECL;	/* FIXME: once the Bison bug requiring this is fixed */

#line 87 "gram.c"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

#include "gram.h"
/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* "end of file"  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_HEAD = 3,                       /* HEAD  */
  YYSYMBOL_BRANCH = 4,                     /* BRANCH  */
  YYSYMBOL_ACCESS = 5,                     /* ACCESS  */
  YYSYMBOL_SYMBOLS = 6,                    /* SYMBOLS  */
  YYSYMBOL_LOCKS = 7,                      /* LOCKS  */
  YYSYMBOL_COMMENT = 8,                    /* COMMENT  */
  YYSYMBOL_DATE = 9,                       /* DATE  */
  YYSYMBOL_BRANCHES = 10,                  /* BRANCHES  */
  YYSYMBOL_DELTATYPE = 11,                 /* DELTATYPE  */
  YYSYMBOL_NEXT = 12,                      /* NEXT  */
  YYSYMBOL_COMMITID = 13,                  /* COMMITID  */
  YYSYMBOL_EXPAND = 14,                    /* EXPAND  */
  YYSYMBOL_GROUP = 15,                     /* GROUP  */
  YYSYMBOL_KOPT = 16,                      /* KOPT  */
  YYSYMBOL_OWNER = 17,                     /* OWNER  */
  YYSYMBOL_PERMISSIONS = 18,               /* PERMISSIONS  */
  YYSYMBOL_FILENAME = 19,                  /* FILENAME  */
  YYSYMBOL_MERGEPOINT = 20,                /* MERGEPOINT  */
  YYSYMBOL_HARDLINKS = 21,                 /* HARDLINKS  */
  YYSYMBOL_USERNAME = 22,                  /* USERNAME  */
  YYSYMBOL_DESC = 23,                      /* DESC  */
  YYSYMBOL_LOG = 24,                       /* LOG  */
  YYSYMBOL_TEXT = 25,                      /* TEXT  */
  YYSYMBOL_STRICT = 26,                    /* STRICT  */
  YYSYMBOL_AUTHOR = 27,                    /* AUTHOR  */
  YYSYMBOL_STATE = 28,                     /* STATE  */
  YYSYMBOL_SEMI = 29,                      /* SEMI  */
  YYSYMBOL_COLON = 30,                     /* COLON  */
  YYSYMBOL_IGNORED = 31,                   /* IGNORED  */
  YYSYMBOL_BRAINDAMAGED_NUMBER = 32,       /* BRAINDAMAGED_NUMBER  */
  YYSYMBOL_LOGIN = 33,                     /* LOGIN  */
  YYSYMBOL_TOKEN = 34,                     /* TOKEN  */
  YYSYMBOL_DATA = 35,                      /* DATA  */
  YYSYMBOL_TEXT_DATA = 36,                 /* TEXT_DATA  */
  YYSYMBOL_NUMBER = 37,                    /* NUMBER  */
  YYSYMBOL_YYACCEPT = 38,                  /* $accept  */
  YYSYMBOL_file = 39,                      /* file  */
  YYSYMBOL_headers = 40,                   /* headers  */
  YYSYMBOL_header = 41,                    /* header  */
  YYSYMBOL_locks = 42,                     /* locks  */
  YYSYMBOL_lock = 43,                      /* lock  */
  YYSYMBOL_lock_type = 44,                 /* lock_type  */
  YYSYMBOL_accesslist = 45,                /* accesslist  */
  YYSYMBOL_logins = 46,                    /* logins  */
  YYSYMBOL_symbollist = 47,                /* symbollist  */
  YYSYMBOL_symbols = 48,                   /* symbols  */
  YYSYMBOL_symbol = 49,                    /* symbol  */
  YYSYMBOL_fscked_symbol = 50,             /* fscked_symbol  */
  YYSYMBOL_name = 51,                      /* name  */
  YYSYMBOL_revisions = 52,                 /* revisions  */
  YYSYMBOL_revtrailer = 53,                /* revtrailer  */
  YYSYMBOL_ignored = 54,                   /* ignored  */
  YYSYMBOL_revision = 55,                  /* revision  */
  YYSYMBOL_date = 56,                      /* date  */
  YYSYMBOL_author = 57,                    /* author  */
  YYSYMBOL_state = 58,                     /* state  */
  YYSYMBOL_branches = 59,                  /* branches  */
  YYSYMBOL_numbers = 60,                   /* numbers  */
  YYSYMBOL_next = 61,                      /* next  */
  YYSYMBOL_opt_number = 62,                /* opt_number  */
  YYSYMBOL_commitid = 63,                  /* commitid  */
  YYSYMBOL_desc = 64,                      /* desc  */
  YYSYMBOL_patches = 65,                   /* patches  */
  YYSYMBOL_patch = 66,                     /* patch  */
  YYSYMBOL_log = 67,                       /* log  */
  YYSYMBOL_text = 68,                      /* text  */
  YYSYMBOL_deltatype = 69,                 /* deltatype  */
  YYSYMBOL_group = 70,                     /* group  */
  YYSYMBOL_kopt = 71,                      /* kopt  */
  YYSYMBOL_owner = 72,                     /* owner  */
  YYSYMBOL_permissions = 73,               /* permissions  */
  YYSYMBOL_filename = 74,                  /* filename  */
  YYSYMBOL_mergepoint = 75,                /* mergepoint  */
  YYSYMBOL_hardlinks = 76,                 /* hardlinks  */
  YYSYMBOL_username = 77,                  /* username  */
  YYSYMBOL_strings = 78                    /* strings  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;




#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_uint8 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && ! defined __ICC && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                            \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if !defined yyoverflow

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* !defined yyoverflow */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  22
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   89

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  38
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  41
/* YYNRULES -- Number of rules.  */
#define YYNRULES  71
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  129

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   292


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,    87,    87,    96,    97,    99,   101,   103,   105,   106,
     108,   109,   111,   114,   115,   117,   119,   120,   122,   139,
     142,   144,   147,   149,   152,   154,   161,   166,   167,   174,
     177,   181,   182,   184,   189,   189,   189,   189,   190,   190,
     190,   190,   190,   192,   220,   225,   228,   231,   234,   243,
     245,   248,   251,   253,   256,   259,   262,   264,   281,   284,
     287,   290,   293,   294,   296,   299,   302,   305,   308,   311,
     314,   315
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if YYDEBUG || 0
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "\"end of file\"", "error", "\"invalid token\"", "HEAD", "BRANCH",
  "ACCESS", "SYMBOLS", "LOCKS", "COMMENT", "DATE", "BRANCHES", "DELTATYPE",
  "NEXT", "COMMITID", "EXPAND", "GROUP", "KOPT", "OWNER", "PERMISSIONS",
  "FILENAME", "MERGEPOINT", "HARDLINKS", "USERNAME", "DESC", "LOG", "TEXT",
  "STRICT", "AUTHOR", "STATE", "SEMI", "COLON", "IGNORED",
  "BRAINDAMAGED_NUMBER", "LOGIN", "TOKEN", "DATA", "TEXT_DATA", "NUMBER",
  "$accept", "file", "headers", "header", "locks", "lock", "lock_type",
  "accesslist", "logins", "symbollist", "symbols", "symbol",
  "fscked_symbol", "name", "revisions", "revtrailer", "ignored",
  "revision", "date", "author", "state", "branches", "numbers", "next",
  "opt_number", "commitid", "desc", "patches", "patch", "log", "text",
  "deltatype", "group", "kopt", "owner", "permissions", "filename",
  "mergepoint", "hardlinks", "username", "strings", YY_NULLPTR
};

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  return yytname[yysymbol];
}
#endif

#ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_int16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292
};
#endif

#define YYPACT_NINF (-95)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-1)

#define yytable_value_is_error(Yyn) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int8 yypact[] =
{
      19,   -20,    -1,   -95,   -95,   -95,   -17,    -4,    32,   -95,
      19,   -95,   -95,   -95,     9,   -95,    10,   -13,     0,   -15,
      11,    12,   -95,   -22,   -95,   -95,   -95,   -95,   -95,   -95,
     -95,   -95,   -95,   -95,    13,    16,    14,   -95,   -95,   -95,
      15,    36,   -95,   -95,    -2,    17,   -95,    18,   -95,    20,
      21,    22,   -95,   -95,   -95,   -95,    23,    24,    25,    27,
     -95,   -95,    31,    28,    37,    26,    29,   -95,    34,    30,
      44,   -95,    33,   -95,   -95,    30,    35,   -20,   -95,   -95,
     -95,   -95,    39,    -9,   -95,    38,    40,    42,   -26,    45,
      46,    47,    43,    48,    48,   -95,   -95,   -95,   -95,   -95,
     -95,   -95,   -95,   -95,   -95,   -95,    41,    52,    53,   -95,
      54,    55,    56,    57,    58,    48,    59,    60,   -95,   -95,
     -95,   -95,   -95,   -95,   -95,   -95,   -95,   -95,   -95
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_int8 yydefact[] =
{
       4,    52,     0,    20,    24,    14,     0,     0,     0,    30,
       4,     8,     9,    51,     0,     7,     0,     0,     0,     0,
       0,     0,     1,     0,     3,     5,     6,    18,    19,    21,
      27,    28,    22,    23,     0,    17,     0,    13,    11,    12,
       0,     0,    29,    56,     0,     0,    10,     0,    54,     0,
       0,     2,    26,    25,    16,    15,     0,     0,     0,     0,
      55,    44,     0,     0,     0,     0,     0,    45,     0,    49,
       0,    58,     0,    57,    46,    49,     0,    52,    31,    59,
      48,    47,     0,    43,    50,     0,     0,     0,     0,     0,
       0,     0,     0,    71,    71,    33,    32,    36,    35,    37,
      34,    38,    40,    39,    41,    42,     0,     0,     0,    63,
       0,     0,     0,     0,     0,    71,     0,     0,    60,    53,
      61,    62,    64,    65,    66,    67,    70,    68,    69
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int8 yypgoto[] =
{
     -95,   -95,    61,   -95,   -95,   -95,   -95,   -95,   -95,   -95,
     -95,   -95,   -95,   -95,   -95,   -95,   -95,   -95,   -95,   -95,
     -95,   -95,   -10,   -95,   -28,   -95,   -95,   -95,   -95,   -95,
     -95,   -95,   -95,   -95,   -95,   -95,   -95,   -95,   -95,   -95,
     -94
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int8 yydefgoto[] =
{
      -1,     8,     9,    10,    19,    37,    46,    11,    17,    12,
      18,    32,    33,    34,    23,    83,    95,    42,    50,    58,
      64,    70,    76,    78,    14,    96,    43,    51,    60,    66,
      73,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     116
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_uint8 yytable[] =
{
     117,    40,    85,   109,    86,   110,    87,    88,    89,    90,
      91,    92,    93,    94,    35,    41,    27,    13,    20,    36,
      28,   126,     1,     2,     3,     4,     5,     6,    15,    29,
      52,    21,    22,     7,    30,    53,    16,    31,    25,    26,
      38,    39,    45,    44,    47,    49,    54,    69,    57,    82,
      48,    65,    61,    63,    72,    55,    77,    56,    62,    59,
      67,    71,    68,    74,    81,    80,     0,    75,    84,    79,
     118,    24,   106,   108,   107,     0,   111,   112,   113,   115,
     114,   119,   120,   121,   122,   123,   124,   125,   127,   128
};

static const yytype_int8 yycheck[] =
{
      94,    23,    11,    29,    13,    31,    15,    16,    17,    18,
      19,    20,    21,    22,    29,    37,    29,    37,    35,    34,
      33,   115,     3,     4,     5,     6,     7,     8,    29,    29,
      32,    35,     0,    14,    34,    37,    37,    37,    29,    29,
      29,    29,    26,    30,    30,     9,    29,    10,    27,    77,
      35,    24,    29,    28,    25,    37,    12,    37,    34,    37,
      29,    35,    34,    29,    29,    75,    -1,    37,    29,    36,
      29,    10,    34,    31,    34,    -1,    31,    31,    31,    31,
      37,    29,    29,    29,    29,    29,    29,    29,    29,    29
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_int8 yystos[] =
{
       0,     3,     4,     5,     6,     7,     8,    14,    39,    40,
      41,    45,    47,    37,    62,    29,    37,    46,    48,    42,
      35,    35,     0,    52,    40,    29,    29,    29,    33,    29,
      34,    37,    49,    50,    51,    29,    34,    43,    29,    29,
      23,    37,    55,    64,    30,    26,    44,    30,    35,     9,
      56,    65,    32,    37,    29,    37,    37,    27,    57,    37,
      66,    29,    34,    28,    58,    24,    67,    29,    34,    10,
      59,    35,    25,    68,    29,    37,    60,    12,    61,    36,
      60,    29,    62,    53,    29,    11,    13,    15,    16,    17,
      18,    19,    20,    21,    22,    54,    63,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    34,    34,    31,    29,
      31,    31,    31,    31,    37,    31,    78,    78,    29,    29,
      29,    29,    29,    29,    29,    29,    78,    29,    29
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_int8 yyr1[] =
{
       0,    38,    39,    40,    40,    41,    41,    41,    41,    41,
      41,    41,    41,    42,    42,    43,    44,    44,    45,    46,
      46,    47,    48,    48,    48,    49,    50,    51,    51,    52,
      52,    53,    53,    53,    54,    54,    54,    54,    54,    54,
      54,    54,    54,    55,    56,    57,    58,    59,    60,    60,
      61,    62,    62,    63,    64,    65,    65,    66,    67,    68,
      69,    70,    71,    71,    72,    73,    74,    75,    76,    77,
      78,    78
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     4,     2,     0,     3,     3,     2,     1,     1,
       4,     3,     3,     2,     0,     3,     2,     0,     3,     2,
       0,     3,     2,     2,     0,     3,     3,     1,     1,     2,
       0,     0,     2,     2,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     7,     3,     3,     3,     3,     2,     0,
       3,     1,     0,     3,     2,     2,     0,     3,     2,     2,
       3,     3,     3,     2,     3,     3,     3,     3,     3,     3,
       2,     0
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (scanner, cvsfile, YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use YYerror or YYUNDEF. */
#define YYERRCODE YYUNDEF


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
# ifndef YY_LOCATION_PRINT
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif


# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value, scanner, cvsfile); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, void *scanner, cvs_file *cvsfile)
{
  FILE *yyoutput = yyo;
  YYUSE (yyoutput);
  YYUSE (scanner);
  YYUSE (cvsfile);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yykind < YYNTOKENS)
    YYPRINT (yyo, yytoknum[yykind], *yyvaluep);
# endif
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, void *scanner, cvs_file *cvsfile)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  yy_symbol_value_print (yyo, yykind, yyvaluep, scanner, cvsfile);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp,
                 int yyrule, void *scanner, cvs_file *cvsfile)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)], scanner, cvsfile);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule, scanner, cvsfile); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif






/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep, void *scanner, cvs_file *cvsfile)
{
  YYUSE (yyvaluep);
  YYUSE (scanner);
  YYUSE (cvsfile);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}






/*----------.
| yyparse.  |
`----------*/

int
yyparse (void *scanner, cvs_file *cvsfile)
{
/* Lookahead token kind.  */
int yychar;


/* The semantic value of the lookahead symbol.  */
/* Default value used for initialization, for pacifying older GCCs
   or non-GCC compilers.  */
YY_INITIAL_VALUE (static YYSTYPE yyval_default;)
YYSTYPE yylval YY_INITIAL_VALUE (= yyval_default);

    /* Number of syntax errors so far.  */
    int yynerrs = 0;

    yy_state_fast_t yystate = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus = 0;

    /* Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss = yyssa;
    yy_state_t *yyssp = yyss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs = yyvsa;
    YYSTYPE *yyvsp = yyvs;

  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead symbol kind.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    goto yyexhaustedlab;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex (&yylval, scanner, cvsfile);
    }

  if (yychar <= YYEOF)
    {
      yychar = YYEOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == YYerror)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = YYUNDEF;
      yytoken = YYSYMBOL_YYerror;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 2: /* file: headers revisions desc patches  */
#line 88 "/home/esr/public_html/cvs-fast-export//gram.y"
                  {
		    /* The description text (if any) is only used
		     * for empty log messages in the 'patch' production
                     */
		     free((void *)cvsfile->description);
		     cvsfile->description = NULL;
		  }
#line 1248 "gram.c"
    break;

  case 5: /* header: HEAD opt_number SEMI  */
#line 100 "/home/esr/public_html/cvs-fast-export//gram.y"
                  { cvsfile->head = atom_cvs_number((yyvsp[-1].number)); }
#line 1254 "gram.c"
    break;

  case 6: /* header: BRANCH NUMBER SEMI  */
#line 102 "/home/esr/public_html/cvs-fast-export//gram.y"
                  { cvsfile->branch = atom_cvs_number((yyvsp[-1].number)); }
#line 1260 "gram.c"
    break;

  case 7: /* header: BRANCH SEMI  */
#line 104 "/home/esr/public_html/cvs-fast-export//gram.y"
                  { warn("ignoring empty branch\n"); }
#line 1266 "gram.c"
    break;

  case 9: /* header: symbollist  */
#line 107 "/home/esr/public_html/cvs-fast-export//gram.y"
                  { cvsfile->symbols = (yyvsp[0].symbol); }
#line 1272 "gram.c"
    break;

  case 11: /* header: COMMENT DATA SEMI  */
#line 110 "/home/esr/public_html/cvs-fast-export//gram.y"
                  { free((yyvsp[-1].s)); }
#line 1278 "gram.c"
    break;

  case 12: /* header: EXPAND DATA SEMI  */
#line 112 "/home/esr/public_html/cvs-fast-export//gram.y"
                { cvsfile->gen.expand = expand_override((yyvsp[-1].s)); }
#line 1284 "gram.c"
    break;

  case 18: /* accesslist: ACCESS logins SEMI  */
#line 123 "/home/esr/public_html/cvs-fast-export//gram.y"
                  {
		    /********************************************************************
		     *	From OPTIONS in rcs(1) man page
		     *
		     *   -alogins
		     *	     Append  the login names appearing in the comma-separated list logins
		     *	     to the access list of the RCS file.
		     *
		     * The logins in the access list seems to be ignored by all RCS operations.
		     * Nevertheless it is appropriate to allow an access list with logins.
		     * Some RCS files have them.  Without this patch you get a syntax error
		     * if you have logins in the access list.	JW 20151120
		     *******************************************************************/
		    (yyval.symbol) = (yyvsp[-1].symbol);
		  }
#line 1304 "gram.c"
    break;

  case 19: /* logins: logins LOGIN  */
#line 140 "/home/esr/public_html/cvs-fast-export//gram.y"
                  { (yyval.symbol) = NULL;		/* ignore LOGIN */ }
#line 1310 "gram.c"
    break;

  case 20: /* logins: %empty  */
#line 142 "/home/esr/public_html/cvs-fast-export//gram.y"
                  { (yyval.symbol) = NULL;		/* empty access list */ }
#line 1316 "gram.c"
    break;

  case 21: /* symbollist: SYMBOLS symbols SEMI  */
#line 145 "/home/esr/public_html/cvs-fast-export//gram.y"
                  { (yyval.symbol) = (yyvsp[-1].symbol); }
#line 1322 "gram.c"
    break;

  case 22: /* symbols: symbols symbol  */
#line 148 "/home/esr/public_html/cvs-fast-export//gram.y"
                  { (yyvsp[0].symbol)->next = (yyvsp[-1].symbol); (yyval.symbol) = (yyvsp[0].symbol); }
#line 1328 "gram.c"
    break;

  case 23: /* symbols: symbols fscked_symbol  */
#line 150 "/home/esr/public_html/cvs-fast-export//gram.y"
                  { (yyval.symbol) = (yyvsp[-1].symbol); }
#line 1334 "gram.c"
    break;

  case 24: /* symbols: %empty  */
#line 152 "/home/esr/public_html/cvs-fast-export//gram.y"
                  { (yyval.symbol) = NULL; }
#line 1340 "gram.c"
    break;

  case 25: /* symbol: name COLON NUMBER  */
#line 155 "/home/esr/public_html/cvs-fast-export//gram.y"
                  {
		  	(yyval.symbol) = xcalloc (1, sizeof (cvs_symbol), "making symbol");
			(yyval.symbol)->symbol_name = (yyvsp[-2].atom);
			(yyval.symbol)->number = atom_cvs_number((yyvsp[0].number));
		  }
#line 1350 "gram.c"
    break;

  case 26: /* fscked_symbol: name COLON BRAINDAMAGED_NUMBER  */
#line 162 "/home/esr/public_html/cvs-fast-export//gram.y"
                  {
		        warn("ignoring symbol %s (FreeBSD RELENG_2_1_0 braindamage?)\n", (yyvsp[-2].atom));
		  }
#line 1358 "gram.c"
    break;

  case 28: /* name: NUMBER  */
#line 168 "/home/esr/public_html/cvs-fast-export//gram.y"
                  {
		    char    name[CVS_MAX_REV_LEN];
		    cvs_number_string (&(yyvsp[0].number), name, sizeof(name));
		    (yyval.atom) = atom (name);
		  }
#line 1368 "gram.c"
    break;

  case 29: /* revisions: revisions revision  */
#line 175 "/home/esr/public_html/cvs-fast-export//gram.y"
                  { *(yyvsp[-1].vlist) = (yyvsp[0].version); (yyval.vlist) = &(yyvsp[0].version)->next;}
#line 1374 "gram.c"
    break;

  case 30: /* revisions: %empty  */
#line 177 "/home/esr/public_html/cvs-fast-export//gram.y"
                  { (yyval.vlist) = &cvsfile->gen.versions; }
#line 1380 "gram.c"
    break;

  case 31: /* revtrailer: %empty  */
#line 181 "/home/esr/public_html/cvs-fast-export//gram.y"
                  { (yyval.atom) = NULL; }
#line 1386 "gram.c"
    break;

  case 32: /* revtrailer: revtrailer commitid  */
#line 183 "/home/esr/public_html/cvs-fast-export//gram.y"
                  { (yyval.atom) = (yyvsp[0].atom); }
#line 1392 "gram.c"
    break;

  case 43: /* revision: NUMBER date author state branches next revtrailer  */
#line 193 "/home/esr/public_html/cvs-fast-export//gram.y"
                  {
		    (yyval.version) = xcalloc (1, sizeof (cvs_version),
				    "gram.y::revision");
		    (yyval.version)->number = atom_cvs_number((yyvsp[-6].number));
		    (yyval.version)->date = (yyvsp[-5].date);
		    (yyval.version)->author = (yyvsp[-4].atom);
		    (yyval.version)->state = (yyvsp[-3].atom);
		    (yyval.version)->dead = !strcmp ((yyvsp[-3].atom), "dead");
		    (yyval.version)->branches = (yyvsp[-2].branch);
		    (yyval.version)->parent = atom_cvs_number((yyvsp[-1].number));
		    (yyval.version)->commitid = (yyvsp[0].atom);
		    if ((yyval.version)->commitid == NULL 
			        && cvsfile->skew_vulnerable < (yyval.version)->date) {
			cvsfile->skew_vulnerable = (yyval.version)->date;
			if (cvsfile->verbose) {
			    char jw_buf[33];
			    warn("skew_vulnerable in file %s rev %s set to %s\n",
				 cvsfile->export_name,
				 cvs_number_string((yyval.version)->number,
						   jw_buf, sizeof(jw_buf)-1),
				 cvstime2rfc3339((yyval.version)->date));
			}
		    }
		    hash_version(&cvsfile->gen.nodehash, (yyval.version));
		    ++cvsfile->nversions;			
		  }
#line 1423 "gram.c"
    break;

  case 44: /* date: DATE NUMBER SEMI  */
#line 221 "/home/esr/public_html/cvs-fast-export//gram.y"
                  {
		    (yyval.date) = lex_date (&(yyvsp[-1].number), scanner, cvsfile);
		  }
#line 1431 "gram.c"
    break;

  case 45: /* author: AUTHOR TOKEN SEMI  */
#line 226 "/home/esr/public_html/cvs-fast-export//gram.y"
                  { (yyval.atom) = (yyvsp[-1].atom); }
#line 1437 "gram.c"
    break;

  case 46: /* state: STATE TOKEN SEMI  */
#line 229 "/home/esr/public_html/cvs-fast-export//gram.y"
                  { (yyval.atom) = (yyvsp[-1].atom); }
#line 1443 "gram.c"
    break;

  case 47: /* branches: BRANCHES numbers SEMI  */
#line 232 "/home/esr/public_html/cvs-fast-export//gram.y"
                  { (yyval.branch) = (yyvsp[-1].branch); }
#line 1449 "gram.c"
    break;

  case 48: /* numbers: NUMBER numbers  */
#line 235 "/home/esr/public_html/cvs-fast-export//gram.y"
                  {
			(yyval.branch) = xcalloc (1, sizeof (cvs_branch),
				    "gram.y::numbers");
			(yyval.branch)->next = (yyvsp[0].branch);
			(yyval.branch)->number = atom_cvs_number((yyvsp[-1].number));
			hash_branch(&cvsfile->gen.nodehash, (yyval.branch));
		  }
#line 1461 "gram.c"
    break;

  case 49: /* numbers: %empty  */
#line 243 "/home/esr/public_html/cvs-fast-export//gram.y"
                  { (yyval.branch) = NULL; }
#line 1467 "gram.c"
    break;

  case 50: /* next: NEXT opt_number SEMI  */
#line 246 "/home/esr/public_html/cvs-fast-export//gram.y"
                  { (yyval.number) = (yyvsp[-1].number); }
#line 1473 "gram.c"
    break;

  case 51: /* opt_number: NUMBER  */
#line 249 "/home/esr/public_html/cvs-fast-export//gram.y"
                  { (yyval.number) = (yyvsp[0].number); }
#line 1479 "gram.c"
    break;

  case 52: /* opt_number: %empty  */
#line 251 "/home/esr/public_html/cvs-fast-export//gram.y"
                  { (yyval.number).c = 0; }
#line 1485 "gram.c"
    break;

  case 53: /* commitid: COMMITID TOKEN SEMI  */
#line 254 "/home/esr/public_html/cvs-fast-export//gram.y"
                  { (yyval.atom) = (yyvsp[-1].atom); }
#line 1491 "gram.c"
    break;

  case 54: /* desc: DESC DATA  */
#line 257 "/home/esr/public_html/cvs-fast-export//gram.y"
                  { cvsfile->description = (yyvsp[0].s); }
#line 1497 "gram.c"
    break;

  case 55: /* patches: patches patch  */
#line 260 "/home/esr/public_html/cvs-fast-export//gram.y"
                  { *(yyvsp[-1].patches) = (yyvsp[0].patch); (yyval.patches) = &(yyvsp[0].patch)->next; }
#line 1503 "gram.c"
    break;

  case 56: /* patches: %empty  */
#line 262 "/home/esr/public_html/cvs-fast-export//gram.y"
                  { (yyval.patches) = &cvsfile->gen.patches; }
#line 1509 "gram.c"
    break;

  case 57: /* patch: NUMBER log text  */
#line 265 "/home/esr/public_html/cvs-fast-export//gram.y"
                  { (yyval.patch) = xcalloc (1, sizeof (cvs_patch), "gram.y::patch");
		    (yyval.patch)->number = atom_cvs_number((yyvsp[-2].number));
		    if (!strcmp((yyvsp[-1].s), "Initial revision\n")) {
			    /* description is available because the
			     * desc production has already been reduced */
			    if (strlen(cvsfile->description) == 0)
				    (yyval.patch)->log = atom("*** empty log message ***\n");
			    else
				    (yyval.patch)->log = atom(cvsfile->description);
		    } else
			    (yyval.patch)->log = atom((yyvsp[-1].s));
		    (yyval.patch)->text = (yyvsp[0].text);
		    hash_patch(&cvsfile->gen.nodehash, (yyval.patch));
		    free((yyvsp[-1].s));
		  }
#line 1529 "gram.c"
    break;

  case 58: /* log: LOG DATA  */
#line 282 "/home/esr/public_html/cvs-fast-export//gram.y"
                  { (yyval.s) = (yyvsp[0].s); }
#line 1535 "gram.c"
    break;

  case 59: /* text: TEXT TEXT_DATA  */
#line 285 "/home/esr/public_html/cvs-fast-export//gram.y"
                  { (yyval.text) = (yyvsp[0].text); }
#line 1541 "gram.c"
    break;

  case 60: /* deltatype: DELTATYPE TOKEN SEMI  */
#line 288 "/home/esr/public_html/cvs-fast-export//gram.y"
                  { (yyval.atom) = (yyvsp[-1].atom); }
#line 1547 "gram.c"
    break;

  case 61: /* group: GROUP IGNORED SEMI  */
#line 291 "/home/esr/public_html/cvs-fast-export//gram.y"
                  { (yyval.atom) = NULL; }
#line 1553 "gram.c"
    break;

  case 64: /* owner: OWNER IGNORED SEMI  */
#line 297 "/home/esr/public_html/cvs-fast-export//gram.y"
                  { (yyval.atom) = NULL; }
#line 1559 "gram.c"
    break;

  case 65: /* permissions: PERMISSIONS IGNORED SEMI  */
#line 300 "/home/esr/public_html/cvs-fast-export//gram.y"
                  { (yyval.atom) = NULL; }
#line 1565 "gram.c"
    break;

  case 66: /* filename: FILENAME IGNORED SEMI  */
#line 303 "/home/esr/public_html/cvs-fast-export//gram.y"
                  { (yyval.atom) = NULL; }
#line 1571 "gram.c"
    break;

  case 67: /* mergepoint: MERGEPOINT NUMBER SEMI  */
#line 306 "/home/esr/public_html/cvs-fast-export//gram.y"
                  { (yyval.number) = (yyvsp[-1].number); }
#line 1577 "gram.c"
    break;


#line 1581 "gram.c"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      yyerror (scanner, cvsfile, YY_("syntax error"));
    }

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval, scanner, cvsfile);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp, scanner, cvsfile);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;


#if !defined yyoverflow
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (scanner, cvsfile, YY_("memory exhausted"));
  yyresult = 2;
  goto yyreturn;
#endif


/*-------------------------------------------------------.
| yyreturn -- parsing is finished, clean up and return.  |
`-------------------------------------------------------*/
yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, scanner, cvsfile);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp, scanner, cvsfile);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif

  return yyresult;
}

#line 317 "/home/esr/public_html/cvs-fast-export//gram.y"


void yyerror(yyscan_t scanner, cvs_file *cvs, const char *msg)
{
    progress_interrupt();
    fprintf(stderr, "%s:%d: cvs-fast-export %s on token %s\n",
	    cvs->export_name, yyget_lineno(scanner),
	    msg, yyget_text(scanner));
}
