/* A Bison parser, made by GNU Bison 3.7.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2020 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

#ifndef YY_YY_GRAM_H_INCLUDED
# define YY_YY_GRAM_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    HEAD = 258,                    /* HEAD  */
    BRANCH = 259,                  /* BRANCH  */
    ACCESS = 260,                  /* ACCESS  */
    SYMBOLS = 261,                 /* SYMBOLS  */
    LOCKS = 262,                   /* LOCKS  */
    COMMENT = 263,                 /* COMMENT  */
    DATE = 264,                    /* DATE  */
    BRANCHES = 265,                /* BRANCHES  */
    DELTATYPE = 266,               /* DELTATYPE  */
    NEXT = 267,                    /* NEXT  */
    COMMITID = 268,                /* COMMITID  */
    EXPAND = 269,                  /* EXPAND  */
    GROUP = 270,                   /* GROUP  */
    KOPT = 271,                    /* KOPT  */
    OWNER = 272,                   /* OWNER  */
    PERMISSIONS = 273,             /* PERMISSIONS  */
    FILENAME = 274,                /* FILENAME  */
    MERGEPOINT = 275,              /* MERGEPOINT  */
    HARDLINKS = 276,               /* HARDLINKS  */
    USERNAME = 277,                /* USERNAME  */
    DESC = 278,                    /* DESC  */
    LOG = 279,                     /* LOG  */
    TEXT = 280,                    /* TEXT  */
    STRICT = 281,                  /* STRICT  */
    AUTHOR = 282,                  /* AUTHOR  */
    STATE = 283,                   /* STATE  */
    SEMI = 284,                    /* SEMI  */
    COLON = 285,                   /* COLON  */
    IGNORED = 286,                 /* IGNORED  */
    BRAINDAMAGED_NUMBER = 287,     /* BRAINDAMAGED_NUMBER  */
    LOGIN = 288,                   /* LOGIN  */
    TOKEN = 289,                   /* TOKEN  */
    DATA = 290,                    /* DATA  */
    TEXT_DATA = 291,               /* TEXT_DATA  */
    NUMBER = 292                   /* NUMBER  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 31 "/home/esr/public_html/cvs-fast-export//gram.y"

    int		i;
    cvstime_t	date;
    char	*s; 		/* on heap */
    const char	*atom;
    cvs_text	text;
    cvs_number	number;
    cvs_symbol	*symbol;
    cvs_version	*version;
    cvs_version	**vlist;
    cvs_patch	*patch;
    cvs_patch	**patches;
    cvs_branch	*branch;
    cvs_file	*file;

#line 117 "gram.h"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif



int yyparse (void *scanner, cvs_file *cvsfile);

#endif /* !YY_YY_GRAM_H_INCLUDED  */
