cvs-fast-export (1.59-1) unstable; urgency=medium

  * New upstream version 1.59
  * Change "cvsreduce" to "cvsstrip" in package description
    to reflect the rename since version 1.56
  * Remove override_dh_python3 used for forced shebang, and
    remove 03-use-python3-only-for-tests.patch too,
    as upstream has switched all python invocations to python3 in 1.59
  * Use dh-sequence-python3 instead of dh-python and "--with python3"
  * Bump Standards-Version to 4.6.0 (no change)

 -- Anthony Fok <foka@debian.org>  Fri, 03 Dec 2021 01:58:34 -0700

cvs-fast-export (1.55-1) unstable; urgency=medium

  * New upstream version 1.55
  * Bump debhelper dependency to "Build-Depends: debhelper-compat (= 13)"
  * Add cppcheck and shellcheck as build-dependencies as per upstream's
    new "buildprep" script
  * Refresh debian/patches

 -- Anthony Fok <foka@debian.org>  Thu, 05 Nov 2020 01:31:11 -0700

cvs-fast-export (1.51-1) unstable; urgency=medium

  * New upstream version 1.51
  * Remove 04-python3-removed-apply.patch which has been applied upstream
  * Refresh other Debian patches

 -- Anthony Fok <foka@debian.org>  Tue, 18 Feb 2020 22:55:30 -0700

cvs-fast-export (1.50-1) unstable; urgency=medium

  * New upstream version 1.50
  * Add John E. Wulff <gitlab@mg.gitlab.com> and update copyright years
    in debian/copyright
  * Refresh Debian patches for new version

 -- Anthony Fok <foka@debian.org>  Wed, 12 Feb 2020 09:23:45 -0700

cvs-fast-export (1.47-2) unstable; urgency=medium

  [ Jelmer Vernooĳ ]
  * Non-maintainer upload.
  * Recommend brz rather than bzr-fastexport.

  [ Anthony Fok ]
  * Ignore quilt dir .pc via .gitignore
  * Patch test suite to use Python3 only
  * Patch cvsconvert and tests/testlifter.py
    to replace apply(f, args) with f(*args) "extended call syntax"
    for full Python 3 compatibility
  * Apply "cme fix dpkg" fixes to debian/control
    - Update debhelper dependency to "Build-Depends: debhelper-compat (= 12)"
    - Bump Standards-Version to 4.5.0 (no change)
  * Bump to debhelper-compat (= 12)
  * debian/control, debian/rules: Migrate to Python3 due to upcoming
    Python2 removal in sid/bullseye (Closes: #936356, #942934)

 -- Anthony Fok <foka@debian.org>  Wed, 12 Feb 2020 08:08:58 -0700

cvs-fast-export (1.47-1) unstable; urgency=medium

  * New upstream release
  * Bump Standards-Version to 4.3.0 (no change)
  * Remove 03-fix-setpython-not-found-error.patch
    which has been applied upstream since upstream version 1.45.
  * debian/docs: Track upstream asciidocified files, i.e.
    NEWS, README, TODO → NEWS.adoc, README.adoc, TODO.adoc

 -- Anthony Fok <foka@debian.org>  Sun, 14 Apr 2019 13:40:39 -0600

cvs-fast-export (1.44-1) unstable; urgency=medium

  * New upstream release
  * Rename debian-branch to debian/sid
  * Apply "cme fix dpkg" fixes to debian/control
  * Bump Standards-Version to 4.1.5 (no change)
  * Use debhelper (>= 11)
  * Adjust for .asc to .adoc renaming of AsciiDoc files
  * Fix "sh: 1: setpython: not found" error in tests/Makefile
  * Add hardening=+all to DEB_BUILD_MAINT_OPTIONS.
    Fixes Lintian info "hardening-no-bindnow usr/bin/cvs-fast-export".
  * Remove ancient "X-Python-Version: >= 2.6" field
  * Replace dh_auto_build’s "make -j1 distclean" with "make -j1 clean"

 -- Anthony Fok <foka@debian.org>  Thu, 12 Jul 2018 10:45:13 -0600

cvs-fast-export (1.43-1) unstable; urgency=medium

  * New upstream release
  * Use debhelper (>= 10)
  * Bump Standards-Version to 4.0.0:
    Use https form of the copyright-format URL in debian/copyright
  * Refresh revdates in 02-reproducible-date.patch
  * Call dh with --no-parallel to avoid doc and test errors

 -- Anthony Fok <foka@debian.org>  Fri, 04 Aug 2017 01:50:49 -0600

cvs-fast-export (1.41-1) unstable; urgency=medium

  * New upstream release
  * Update debian/copyright for 2017

 -- Anthony Fok <foka@debian.org>  Sun, 05 Mar 2017 23:28:18 -0700

cvs-fast-export (1.40-1) unstable; urgency=medium

  * New upstream release
  * Build-Depends on the newly split asciidoc-base package rather than
    the asciidoc package to avoid pulling in texlive packages.
  * Bump Standards-Version to 3.9.8 (no change)
  * Use secure https URLs for Vcs-* fields
  * Update copyright years in debian/copyright
  * Refresh Debian patches
  * Ignore quilt dir

 -- Anthony Fok <foka@debian.org>  Fri, 30 Dec 2016 06:50:15 -0700

cvs-fast-export (1.35-1) unstable; urgency=medium

  * New upstream release 1.35
     - Properly handle CVS masters with nonempty access lists.
     - Bail out gracefully on pathological masters with no revisions.
    Special thanks to John E. Wulff for the fix. (Closes: #801110)
  * Edit Makefile to respect CFLAGS from the environment so that
    DEB_BUILD_OPTIONS=noopt and hardening flags are honoured.
    (Closes: #801108)
  * Comment out -march=native in Makefile to prevent FTBFS on some platforms
    and other potential incompatibility problems.
  * Fix reproducible issues with Asciidoc-generated timestamp.

 -- Anthony Fok <foka@debian.org>  Tue, 15 Dec 2015 11:41:36 -0700

cvs-fast-export (1.34-1) unstable; urgency=medium

  * Initial release (Closes: #773316).  Upstream version 1.34 contains:
     - Another Python compatibility fix.
     - Fix to inconsistent license header in revdir.c.

 -- Anthony Fok <foka@debian.org>  Fri, 25 Sep 2015 12:31:28 -0600

cvs-fast-export (1.29-1) UNRELEASED; urgency=low

  * First attempt at Initial release, rejected due to inconsistent
    license in revdir.c (GPL-2) vs the rest of the package (GPL-2+).
    (would have closed #773316)
  * Cherry-pick minor post-1.29 fixes from upstream.

 -- Anthony Fok <foka@debian.org>  Thu, 18 Dec 2014 14:52:09 -0700
